'''Command to display configuration'''
import logging
import click
from rich.console import Console
from rich.panel import Panel

logger = logging.getLogger(__name__)

@click.command()
@click.pass_context
def config(ctx):
    '''Display configuration'''
    config = ctx.obj['config']
    console = Console()

    try:
        for section in config:
            if section == "DEFAULT":
                continue

            output = ''
            for key in config[section]:
                output += key + ' [blue]' + config[section][key] + '[/blue]\n'

            panel = Panel(output, title=section, border_style='blue')
            console.print(panel)
    except KeyError:
        logger.exception('Config does not contain key')
