'''Command to copy necessary files'''
import logging
import shutil
import configparser
import click
from rich.progress import track

logger = logging.getLogger(__name__)

@click.command()
@click.pass_context
def copy(ctx):
    '''Copy necessary files into projects'''
    config = ctx.obj['config']
    file_key = 'Files'

    try:
        for file in track(config[file_key], description='Copying files...'):
            root_path = ctx.obj['root_path']
            source_path = root_path + '/' + config[file_key][file]
            target_path = source_path.replace(config['Paths']['base_path'] + '/', '')

            shutil.copy(source_path, target_path)
    except KeyError:
        logger.exception('Config does not contain section')
    except configparser.InterpolationMissingOptionError:
        logger.exception('Config file contains faulty interpolation')
    except FileNotFoundError:
        logger.exception('Could not find source file')
    except PermissionError:
        logger.exception('Could not write to target directory')
