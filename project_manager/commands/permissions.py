'''Command to set permissions'''
import os
import logging
import subprocess
import click

logger = logging.getLogger(__name__)

@click.command()
@click.pass_context
def permissions(ctx):
    '''Set permissions on repositories'''
    config = ctx.obj['config']
    config_key = 'Permissions'
    path = ctx.obj['root_path']

    try:
        for directory in config[config_key]:
            if not os.path.isdir(directory):
                continue
            uid = 1000
            gid = config[config_key][directory]
            target_dir = os.path.abspath(os.path.join(path, directory))

            subprocess.run(f"sudo chown -R {uid}:{gid} {target_dir}",
                    shell=True, check=True, stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL)
            subprocess.run(f"sudo chmod -R 775 {target_dir}",
                    shell=True, check=True, stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL)
    except KeyError:
        logger.exception('Config does not contain section')
    except subprocess.CalledProcessError:
        logger.exception('Subprocess failed')
