'''Command to install dependencies'''
import os
import logging
import subprocess
import click
from rich.console import Console

logger = logging.getLogger(__name__)

@click.command()
@click.option('-r', '--repository')
@click.pass_context
def install(ctx, repository):
    '''Install dependencies'''
    config = ctx.obj['config']
    config_key = 'Repositories'
    working_dir = os.getcwd()
    console = Console()

    try:
        with console.status('Installing dependencies...'):
            if repository:
                install_dependencies(repository, working_dir)
                return

            for repo in config[config_key]:
                install_dependencies(repo, working_dir)
    except KeyError:
        logger.exception('Config does not contain section')
    except subprocess.CalledProcessError:
        logging.critical('Subprocess failed')


def install_dependencies(repository: str, working_dir):
    '''Install dependencies'''
    if not os.path.isdir(repository):
        logger.warning("%s is not a directory or does not exist", repository)
        return

    os.chdir(repository)
    if os.path.isfile('composer.json'):
        subprocess.run('composer install', shell=True, check=True,
                stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)

    if os.path.isfile('package.json'):
        subprocess.run('npm install', shell=True, check=True,
                stderr= subprocess.DEVNULL, stdout=subprocess.DEVNULL)

    os.chdir(working_dir)
    return
