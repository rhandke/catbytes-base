'''Commad to clone repositories'''
import os
import logging
import subprocess
import click
from rich.progress import track

logger = logging.getLogger(__name__)

@click.command()
@click.pass_context
def clone(ctx):
    '''Clone project repositories'''
    config = ctx.obj['config']
    config_key = 'Repositories'

    try:
        for repo in track(config[config_key],
                description="Cloning Repositories"):
            if os.path.isdir(repo):
                continue

            uri = config[config_key][repo]
            subprocess.run(f"git clone {uri} {repo}",
                    shell=True, check=True, text=True, stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL)
    except KeyError:
        logger.exception('Config does not contain section')
    except subprocess.CalledProcessError:
        logger.exception('Subprocess failed')
