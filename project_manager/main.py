'''Project Manager'''
from configparser import ConfigParser, ExtendedInterpolation
import os
import logging
import click
from rich.console import Console
from rich.text import Text
from rich.logging import RichHandler

logging.basicConfig(
    level="NOTSET",
    format="%(message)s",
    datefmt="[%X]",
    handlers=[RichHandler(rich_tracebacks=True)]
)

logger = logging.getLogger(__name__)

PROJECT_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
PLUGIN_FOLDER = os.path.join(os.path.dirname(__file__), 'commands')

class ProjectManager(click.MultiCommand):
    '''ProjectManager'''
    def list_commands(self, ctx):
        commands = []
        for filename in os.listdir(PLUGIN_FOLDER):
            if filename.endswith('.py'):
                commands.append(filename[:-3])
        commands.sort()
        return commands

    def get_command(self, ctx, cmd_name):
        command = {}
        func = os.path.join(PLUGIN_FOLDER, cmd_name + '.py')
        with open(func) as file:
            code = compile(file.read(), func, 'exec')
            eval(code, command, command)
        return command[cmd_name]


@click.group(cls=ProjectManager)
@click.pass_context
def cli(ctx):
    '''Manage the project environment'''
    config_file = 'config.ini'

    if not os.path.isfile(config_file):
        logger.error("Could not find %s" %(config_file))
        return

    config = ConfigParser(interpolation=ExtendedInterpolation())
    config.read(config_file)
    ctx.obj = {}
    ctx.obj['config'] = config
    ctx.obj['root_path'] = PROJECT_ROOT
