'''Project Manager'''
import sys
from configparser import ConfigParser
from setuptools import setup

CONFIG_FILE = 'project.cfg'
config = ConfigParser()
if not config.read(CONFIG_FILE):
    sys.exit(f"Could not read {CONFIG_FILE}")


setup(
    name=config.get('GENERAL', 'ProjectName'),
    version=config.get('GENERAL', 'Version'),
    py_modules=['helper'],
    install_requires=[
        'Click',
        'Rich',
    ],
    entry_points={'console_scripts': [f"{config.get('GENERAL', 'CommandName')}=project_manager.main:cli"]})
