# Catbytes
Base repository for the docker-compose environment and additional tools.
Provides a basic CLI to manage the project by:
* cloning the projects (e.g. api, frontend, backend)
* installing the dependencies of one or all projects

# Configuration
To configure the base app and the project managing CLI simply edit the project.cfg file This file will be read using the configparser python library and expects to be written in *.ini style.

# Installation
```
git clone git@gitlab.com:rhandke/catbytes-base.git catbytes
cd catbytes
pip3 install .
```

# Usage
```
cb --help
```
The command_name `cb` can be configured in the project.cfg. Make sure to rerun the pip installation afterwards
```
pip3 install .
```
